module.exports = function (gulp, $, DIR, CONFIG, PRODUCTION) {

    // currenty environment
    $.util.log($.util.colors.green(!PRODUCTION || 'PRODUCTION'));


    return function () {
        gulp.src(CONFIG.src.js, {cwd: DIR})

        // create sourcemaps
            .pipe($.if("*.js", $.sourcemaps.init()))

            // include requires all files
            .pipe($.include({
                extensions: "js",
                includePaths: [
                    "./bower_components",
                    "./node_modules",
                    DIR + "src/js"
                ]
            }))

            // catch errors
            .on('error', $.onError)

            // save before renaming
            .pipe(gulp.dest(CONFIG.dest.js, {cwd: DIR}))

            // minify if PRODUCTION
            .pipe(PRODUCTION ? $.if("*.js", $.uglify()) : $.util.noop())

            // rename if production
            .pipe(PRODUCTION ? $.rename(function (path) {
                if (path.extname == '.js') {
                    path.basename += ".min";
                }
            }) : $.util.noop())

            .pipe($.debug({
                title: "JS file Changed: "
            }))

            // write to sourcemap
            .pipe($.if("*.js", $.sourcemaps.write('./')))

            // put results to distination folder
            .pipe(gulp.dest(CONFIG.dest.js, {cwd: DIR}))
    }
}