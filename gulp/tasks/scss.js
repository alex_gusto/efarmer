module.exports = function (gulp, $, dir, CONFIG, PRODUCTION) {

    return function () {

        // get scss files
        gulp.src(CONFIG.src.css, { cwd: dir })

            // enter changes files
            .pipe($.debug({ title: 'File changed:' }))

            // create sourcemaps
            .pipe($.sourcemaps.init())

            // compiling to css
            .pipe($.sass({
                includePaths: ['bower_components']
            }).on('error', $.onError))

            // uglify css
            .pipe(PRODUCTION ? $.shorthand() : $.util.noop())

            // adding vendor prefixes and minify if PRODUCTION
            .pipe(PRODUCTION ? $.postcss([$.cssnext, $.cssnano, $.flex]) : $.util.noop())

            // change name to main.min.css if PRODUCTION
            .pipe(PRODUCTION ? $.rename({ suffix: ".min" }) : $.util.noop())

            // write to sourcemap
            .pipe($.sourcemaps.write('./'))

            // put result to destination
            .pipe(gulp.dest(CONFIG.dest.css, { cwd: dir }))
    }
};