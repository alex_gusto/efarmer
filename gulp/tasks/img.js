module.exports = function (gulp, $, dir, CONFIG, PRODUCTION) {

    return function () {

        gulp.src(CONFIG.src.img, {cwd: dir})

            .pipe($.changed(CONFIG.src.img, {cwd: dir}))

            .pipe($.debug({title: 'Changed'}))

            // minify if PRODUCTION
            .pipe(PRODUCTION ? $.imagemin() : $.util.noop())

            .pipe(gulp.dest(CONFIG.dest.img, {cwd: dir}))


    }
}