module.exports = function (gulp, $, dir, CONFIG, PRODUCTION) {

    return function () {

        // get pug files
        gulp.src(CONFIG.src.html, { cwd: dir })

            .pipe($.plumber())

            .pipe($.pugI18n({

                i18n: {
                    dest: CONFIG.dest.html,
                    namespace: '$locales',
                    locales: dir + 'src/views/*.*'
                },

                pretty: !PRODUCTION
            }))

            // replace names of files if PRODUCTION
            .pipe(PRODUCTION ? $.inject.replace('main.css', 'main.min.css') : $.util.noop())
            .pipe(PRODUCTION ? $.inject.replace('main.js', 'main.min.js') : $.util.noop())

            // put results to destination
            .pipe(gulp.dest(CONFIG.dest.html, { cwd: dir }))
    }
}