module.exports = function (gulp, $, dir, CONFIG) {

    $.browserSync.use($.htmlInjector);
    $.browserSync.init(CONFIG.server);


    // reload browser after changing files
    $.browserSync.watch(CONFIG.dest.css + "*.css", { cwd: dir }).on('change', $.browserSync.reload);
    $.browserSync.watch(CONFIG.dest.html + "**", { cwd: dir }).on('change', $.browserSync.reload);
    $.browserSync.watch(CONFIG.dest.img + "*.*", { cwd: dir }).on('change', $.browserSync.reload);
    $.browserSync.watch(CONFIG.dest.js + "*.js", { cwd: dir }).on('change', $.browserSync.reload);
}
