module.exports = function (gulp, $, dir, CONFIG) {


    // watch html and json(views) files
    gulp.watch(CONFIG.watch.html, { cwd: dir }, ['html']);

    // watch js files changes
    gulp.watch(CONFIG.watch.js, { cwd: dir }, ['scripts']);

};