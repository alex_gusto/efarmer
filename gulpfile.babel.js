/**
 * Created by Alex Gusto.
 * Skype: sania-jack
 */

import gulp from 'gulp';
import del from 'del';
import minimist from 'minimist';
import loadPlugins from 'gulp-load-plugins';

// load all plugins for project
const $ = loadPlugins({
    rename: {
        'gulp-i18n-pug': 'pugI18n',
        'gulp-inject-string': 'inject'
    }
});

$.onError = require('./gulp/utils/error');
$.browserSync = require('browser-sync').create();
$.htmlInjector = require('bs-html-injector');
$.cssnano = require('cssnano');
$.cssnext = require('postcss-cssnext');
$.flex = require('postcss-flexibility');


/**
 * Defined environments variables
 * to change env write to command line gulp --env=production
 */
var defaultOptions = {
    string: ['env', 'dir'],
    default: {
        env: 'development',
        dir: './'
    }
};

const OPTIONS = minimist(process.argv.slice(2), defaultOptions);

// check cwd
if (__dirname != process.env.INIT_CWD) {
    OPTIONS.dir += process.env.INIT_CWD.replace(__dirname, '').slice(1) + '/';
}

// get CONFIG
const CONFIG = require(OPTIONS.dir + 'gulp.config.json');

/**
 *
 * @type {boolean}
 */
var PRODUCTION = OPTIONS.env === 'production';


// Build scss
gulp.task('scss', _getModule('task', 'scss'));

// Build js
gulp.task('scripts', _getModule('task', 'scripts'));

// Build html
gulp.task('html', _getModule('task', 'html'));

// Build images
gulp.task('img', _getModule('task', 'img'));

// Server for development
gulp.task('server', function () {
    return _getModule('util', 'server')
});

gulp.task('watcher', function () {

    // current environment
    $.util.log($.util.colors.green(OPTIONS.env.toUpperCase()));

    return _getModule('util', 'watcher');
});

gulp.task('scss:watch', function () {
    return $.watch(CONFIG.watch.css, { cwd: OPTIONS.dir }, function () {
        gulp.start('scss');
    });
});

gulp.task('img:watch', function () {
    return $.watch(CONFIG.watch.img, { cwd: OPTIONS.dir }, function () {
        gulp.start('img');
    });
});

gulp.task('default', ['build','server', 'watch', 'watcher']);

// Watch for changes
gulp.task('watch', ['scss:watch', 'img:watch']);

// Build all files
gulp.task('build', ['scss', 'scripts', 'html', 'img']);

// clean destination directory
gulp.task('clean', function (cb) {
    del.sync(OPTIONS.dir + CONFIG.clean, function (err) {
        cb(err);
    });

    $.util.log($.util.colors.red(OPTIONS.dir + CONFIG.clean + ' was cleaned!!!'));
});

/**
 *
 * @param {string} type - type of gulp module task or util.
 * @returns {string} name - module name
 */
function _getModule(type, name) {
    let pathTo;

    switch (type) {
        case 'util':
            pathTo = './gulp/utils/';
            break;
        case 'task':
            pathTo = './gulp/tasks/';
            break;
        default:
            return $.util.log('Set correct module type.');
            break;
    }

    return require(pathTo + name)(gulp, $, OPTIONS.dir, CONFIG, PRODUCTION);
}




