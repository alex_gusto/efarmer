Gulp bundler
==========================
GIT: git@bitbucket.org:alex_gusto/efarmer.git

### Работа с gulp
Для работы с шаблоном используем следующие команды:
```
gulp build
```
* Разработка. Поднимается сервер на 3001 порту(по-умолчанию), запускается watcher для файлов:
```
gulp
```
* Продакшин. Перед заливкой на сервер, файлы с папки dist/ минифицируются:
```
gulp build --env=production
```
* Очистка папки dist/:
 ```
 gulp clean
 ```
  

## Modernizr https://modernizr.com/

```
npm run modernizr
```
Команда скачает новый modernizr.js изходя из modernizr-config

  