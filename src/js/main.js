// jQuery
//=require jquery/dist/jquery.js


// Bootstrap modules
//=require bootstrap/js/dist/util.js
//=require bootstrap/js/dist/carousel.js
//=require bootstrap/js/dist/collapse.js

// Modernizr
//=require modules/modernizr.js

var _JS = window._JS || {};

_JS.init = function () {

    try {

        this.initSliders();

        this.scrollTo();

        this.checkBrowserVersion();

    } catch (err) {

        console.error(err);
    }

};

_JS.initSliders = function () {

    $('#uspSlider').carousel();

    $('#homeTestimonialsSlider').carousel();

};



_JS.scrollTo = function () {


    $('a[href="#homeVideo"]').on('click', onClickEvent);
    $('a[href="#pricingSection"]').on('click', onClickEvent);

    function onClickEvent(event) {
        event.preventDefault();
        var elementID = $(this).attr('href');

        scrollTo(elementID);
        iframeManage(elementID);

    }

    function iframeManage(iframeID) {
        var iframeParams;

        if ($(iframeID)[0] && $(iframeID)[0].tagName === 'IFRAME') {

            // Get all iframe params from src
            iframeParams = _JS.urlParse($(iframeID)[0].src);

            // Start play
            if (!iframeParams.hasOwnProperty('autoplay')) {
                $(iframeID)[0].src += "&autoplay=1";
            } else {
                $(iframeID)[0].src = $(iframeID)[0].src.replace('autoplay=0', 'autoplay=1')
            }
        }
    }

    function scrollTo(elementID) {

        $('html, body').animate({
            scrollTop: $(elementID).offset().top - $('#header').height()
        }, 1000);
    }
};

_JS.checkBrowserVersion = function(){

    if(!Modernizr.flexbox && !Modernizr.flexwrap){
        window.location = 'https://browsehappy.com/?locale=en';
    }

};

// method for parsing url
_JS.urlParse = function (url) {
    url = url.split('?')[1];

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(url)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
};


$(_JS.init());